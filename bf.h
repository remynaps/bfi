#include <iostream>

#ifndef BF_H
#define BF_H

class Brainfuck
{
	private:
		char data[30000];
    	char *dataPointer;

	public:

		Brainfuck();

		void add();

		void substract();

		void print();

		void getInput();

		void nextCell();

		void prevCell();

		void startLoop();

		void endloop();
};

#endif