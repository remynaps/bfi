#include "bf.h"

using namespace std;

Brainfuck::Brainfuck()
{
    dataPointer = data;
}

void Brainfuck::add()
{
	(*dataPointer)++;
}

void Brainfuck::substract()
{
	(*dataPointer)--;
}

void Brainfuck::print()
{
	cout << *dataPointer;
}

void Brainfuck::getInput()
{
	cin >> *dataPointer;
}

void Brainfuck::nextCell()
{
	dataPointer++;
}

void Brainfuck::prevCell()
{
	dataPointer--;
}

void Brainfuck::startLoop()
{

}

void Brainfuck::endloop()
{

}
