#include "bf.h"

using namespace std;


void interpretCode(char code[], Brainfuck *instance)
{
	char *curChar = code;
	while (curChar) 
	{
	    switch (curChar) 
	    {
		    case '>':
	            instance->nextCell();
	            break;
	        case '<':
	            instance->prevCell();
	            break;
	        case '+':
	            instance->add();
	            break;
	        case '-':
	            instance->substract();
	            break;
	        case '.':
	            instance->print();
	            break;
	        case ',':
	            instance->getInput();
	            break;
	    }
	}
	curChar++;
}

int main()
{
	Brainfuck instance = new Brainfuck();
	interpretCode("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++.", *instance);
	return 0;
}
